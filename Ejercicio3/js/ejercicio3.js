var op=[["Opción1"],["Opción2"],["Opción3"],["Opción4"]]; //vector donde almacenamos la respuesta
var PyR=[["Preguntas"],["Respuestas"]]; //vector donde almacenarameos todos los datos

//declaramos las preguntas
PyR[0].push("¿En que periodo se dio la primera guerra mundial?");
PyR[0].push("¿Cual es la capital de canada?");
PyR[0].push("¿Cuál es el rio más grande del mundo?");
PyR[0].push("¿En qué se especializa la cartografía?");
PyR[0].push("¿Que tipo de animal es la ballena?");
PyR[0].push("¿Como se llama el dios Egipcio del sol?");
PyR[0].push("¿Quién ganó el mundial de 2018?");
PyR[0].push("¿cuantos huesos tiene el cuerpo humano?");
PyR[0].push("¿Cuánto vale el número pi?");
PyR[0].push("¿Quien escribio el libro Jesucristo Liberador?");

//asignamos las respuestas correctas
PyR[1].push("1914-1918");
PyR[1].push("Ottawa");
PyR[1].push("El Amazonas");
PyR[1].push("Es la ciencia que estudia los mapas.");
PyR[1].push("Mamífero");
PyR[1].push("Ra");
PyR[1].push("Francia");
PyR[1].push("206");
PyR[1].push("3,1416");
PyR[1].push("Jon Sobrino");


///////////////////////////////////////
op[0].push("1914-1918");
op[1].push("1945-1948");
op[2].push("1930-1935");
op[3].push("1912-1913");
///////////////////////////////////////
op[0].push("Ottawa");
op[1].push("Nueva York");
op[2].push("Toronto");
op[3].push("Vancouver");
///////////////////////////////////////
op[0].push("El Rio Atlántico");
op[1].push("El Amazonas");
op[2].push("El Rio Paz");
op[3].push("El Nilo");
///////////////////////////////////////
op[0].push("Es la ciencia que estudia los mapas.");
op[1].push("Es la ciencia que estudia la vida.");
op[2].push("Es la ciencia que estudia los animales.");
op[3].push("Ninguna de las anteriores.");
///////////////////////////////////////
op[0].push("Mamífero");
op[1].push("Pez");
op[2].push("Anfibio");
op[3].push("Marino");
///////////////////////////////////////
op[0].push("Ra");
op[1].push("Solesin");
op[2].push("Solenius");
op[3].push("Grelis");
///////////////////////////////////////
op[0].push("Francia");
op[1].push("Argentina");
op[2].push("Croacia");
op[3].push("Alemania");
///////////////////////////////////////
op[0].push("1000");
op[1].push("206");
op[2].push("312");
op[3].push("216");
///////////////////////////////////////
op[0].push("3,1416");
op[1].push("3.1516");
op[2].push("3.15");
op[3].push("Nariz");
///////////////////////////////////////
op[0].push("Ignacio Ellacuria");
op[1].push("Jon Sobrino");
op[2].push("Monseñor Romero");
op[3].push("Juan Pablo II");


// parte procedimental
var contador=1;
var r;
var pregunta;
var opcion;
var opcionS;

var respU=[];
var indice=[];
var i;

var puntaje=0;

while (contador<=10){
 contador+=1;
    opcion=null;
    r=(Math.floor(Math.random() *10) + 1);
    
    pregunta=PyR[0][r];
    indice.push(r);
    
    for(i=0;i<4;i++){
        
        pregunta+=("\n"+i +"- "+ op[i][r]);
        
    }
    
    while(opcion==null  || (opcion!=0 && opcion!=1 && opcion!=2 && opcion!=3) || isNaN(opcion)){
    
        opcion=parseInt(prompt(pregunta));
    
    }
    opcionS=op[opcion][r];
    
    
    respU.push(opcionS);
    
    if(opcionS==PyR[1][r]){
        
        puntaje=puntaje+10;
        
    }else{
        
    }
    
    
}

document.write("<div class='contenedor'>");
document.write("<h2>Resultados</h2>");
document.write("<table ><tr><th>Preguntas</th><th>Respuesta esperada</th><th>Su respuesta</th></tr>");

var aux;
for(i=0;i<10;i++){
    aux=indice[i];
    
    document.write("<tr>");
    document.write("<td>"+PyR[0][aux]+"</td>");
    document.write("<td>"+PyR[1][aux]+"</td>");
    document.write("<td>"+respU[i]+"</td>");
    document.write("</tr>");
    
}
var nota;
nota= puntaje/100;
document.write("</table>");
document.write("<p class='pfin'>Nota: "+puntaje+"/100 </p>");
document.write("</div>");




