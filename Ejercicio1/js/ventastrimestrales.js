/****************************************************/
/*Materia: Lenguajes Interpretados en el cliente */
/*Guía2 : Repetitivas y arreglos */
/*Ejemplo: 4 - Promedio de Ventas */
/****************************************************/

//Declaramos una matriz de 5 filas 1 columna
var ventastrim=[["Galerias"],["Metrocentro"],["MultiPlaza"],["Plaza Madero"],["Plaza   Futura"]];
var mayor=0.0, menor=0.0, promedio=0.0;
var i,j, monto=0.0; 
var imayor=0, jmayor=1, imenor=0, jmenor=1;

//Variables discusión.

var promS=[]; //Declarar arreglo para guardar el promedio.
var pro=0.0;// Variable a utilizar para calcular promedio.

//arreglos para mayor y menor venta de cada sucursal.
var mayorVS=[];
var menorVS=[];



//Se solicitan las ventas trimestrales de cada sucursal

for(i=0; i<5;i++){

    
    
    for(j=1; j<=4;j++){

        do{

            monto=parseFloat(prompt("Ventas totales de sucursal " + ventastrim[i][0] + " en trimestre " + j + "$:"));

        }while (isNaN(monto) || monto<=0);//Validamos la entrada

        ventastrim[i].push(monto);
        

    }

}



//Calcular promedio de las sucursales.
for(i=0; i<5;i++){

    
    pro=0.0;
    for(j=1; j<=4;j++){

       
        pro=pro+ventastrim[i][j];

    }
    pro=pro/4
    promS.push(pro);
    

}

//Calcular mayor y menor venta de cada sucursal.
var aux;
var aux2;

for (i=0;i<5;i++){
    aux=ventastrim[i][1];
    aux2=ventastrim[i][1];
    for (j=1;j<=4;j++){
        
        if(aux<ventastrim[i][j+1]){
            aux=ventastrim[i][j+1]
           
            }
          
        if(aux2>ventastrim[i][j+1]){
            
           
             aux2=ventastrim[i][j+1]
            
        }
        
     }
    mayorVS.push(aux);
    menorVS.push(aux2);
    
}

///



//Hacemos los calculos y mostramos datos
var m1="<tr>", m2="</td><td>", m3="</td></tr>";

//Definimos primer valor de comparación
mayor=ventastrim[0][1]; menor=ventastrim[0][1];

for(i=0; i<5;i++){
document.writeln("<tr>");
document.writeln("<td>" + ventastrim[i][0] + "</td>");

    for(j=1; j<=4;j++){

        //Calculamos mayor venta

        if(ventastrim[i][j]>mayor){

            mayor=ventastrim[i][j];

            //Guardamos la posición

            imayor=i;

            jmayor=j;

        }

        //Calculamor menor venta

        if(ventastrim[i][j]<menor){

            menor=ventastrim[i][j];

            //Guardamos la posición

            imenor=i;

            jmenor=j;

        }

        promedio +=ventastrim[i][j];

        document.writeln("<td class='monto'>$" + ventastrim[i][j].toFixed(2) + "</td>");
       

    }
    
    
    
             document.writeln("<td class='monto'>$" +promS[i] + "</td>");
             document.writeln("<td class='monto'>$" +mayorVS[i] + "</td>");
             document.writeln("<td class='monto'>$" +menorVS[i] + "</td>");
}





promedio /=20; //Calculamos el promedio total de ventas

document.writeln("</tr></tbody></table></div>");
document.writeln('<div class="tbl-header"><table><thead><tr>');
document.writeln('<th>Venta mayor</th><td class="monto">$'+ mayor.toFixed(2)
+'</td>'+'<td>'+ ventastrim[imayor][0] +'</td>'+'<td>Trimestre '+ jmayor
+'</td></tr>');
document.writeln('<tr><th>Venta menor</th><td class="monto">$'+ menor.toFixed(2)
+'</td>'+'<td>'+ ventastrim[imenor][0] +'</td>'+'<td>Trimestre '+ jmenor +'</td>');
document.writeln('<tr><th>Promedio total</th><td colspan="4">$'+ promedio.toFixed(2)
+'</td>');
document.writeln('</tr></tbody></table></div>');


//arreglo que guarda nombres de sucursales.
var ventastrim2= ["Galerias","Metrocentro","MultiPlaza","Plaza Madero","Plaza Futura"]



var aux3;
var aux4;
//for para ordenar los promedios
for(i=0;i<5;i++){
    for(j=i+1;j<5;j++){
    
        if(promS[j]>promS[i]){
            aux3=promS[i];
            promS[i]=promS[j];
            promS[j]=aux3;
            
            aux4=ventastrim2[i];
            ventastrim2[i]=ventastrim2[j];
            ventastrim2[j]=aux4;
            
        }
    
    
}

}
//Mostrar sucursal y su promedio en orden.

document.writeln("<p class='hey'><strong>Promedio de Sucursales</strong></p>");
document.writeln('<table>');
for(i=0;i<5;i++){
    document.writeln('<tr><th class="hr"><strong>Sucursal</strong></th><th class="hr"></strong >Promedio</strong></th></tr><tr>');
    document.writeln('<td>'+ventastrim2[i]+'</td>');
    document.writeln('<td>$'+promS[i]+'</td>');
    
    
}

document.writeln('</tr></table>');

















